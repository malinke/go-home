// Copyright 2019 Max Linke
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/d2r2/go-dht"
	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
)

type Result struct {
	Temperature float32
	Humidity    float32
}

func readTemp(sensor dht.SensorType, pin int) (res Result, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	temperature, humidity, _, err :=
		dht.ReadDHTxxWithContextAndRetry(ctx, sensor, pin, true, 10)
	if err != nil {
		return Result{-300, -1}, err
	}
	return Result{temperature, humidity}, err
}

func TempAnswer(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	sensorType := dht.DHT22
	pin := 4
	res, err := readTemp(sensorType, pin)
	if err != nil {
		w.WriteHeader(501)
		fmt.Fprint(w, "[]")
		return
	}
	j, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(501)
		fmt.Fprint(w, "[]")
		return
	}
	w.WriteHeader(200)
	fmt.Fprintf(w, "%s", j)

}


func main() {
	router := httprouter.New()
	router.GET("/read", TempAnswer)
	handler := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodHead,
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
		},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: false,
		Debug: true,
	}).Handler(router)
	log.Fatal(http.ListenAndServe(":8666", handler))
}
